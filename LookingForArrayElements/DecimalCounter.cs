﻿using System;

#pragma warning disable S2368

namespace LookingForArrayElements
{
#pragma warning disable
    public static class DecimalCounter
    {
        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that match the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges)
        {
            if (arrayToSearch == null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges == null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                decimal[] range = ranges[i] ?? throw new ArgumentNullException(nameof(ranges));
                if (range.Equals(Array.Empty<decimal>()))
                    continue;
                if (range.Length == 0)
                {
                    return 0;
                }

                if (range.Length != 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.", nameof(ranges));
                }

                if (range[0] > range[1])
                {
                    throw new ArgumentException("error", nameof(ranges));
                }
            }

            int total = 0;
            for (int i = 0; i < arrayToSearch.Length; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    decimal[] range = ranges[j] ?? throw new ArgumentNullException(nameof(ranges));
                    if (range.Equals(Array.Empty<decimal>()))
                        continue;
                    if (arrayToSearch[i] >= range[0] && arrayToSearch[i] <= range[1])
                    {
                        total++;
                        break;
                    }
                }
            }

            return total;
        }


        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that match the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges, int startIndex, int count)
        {
            if (arrayToSearch == null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges == null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                decimal[] range = ranges[i] ?? throw new ArgumentNullException(nameof(ranges));
                if (range.Equals(Array.Empty<decimal>()))
                    continue;
                if (range.Length == 0)
                {
                    return 0;
                }

                if (range.Length != 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.", nameof(ranges));
                }

                if (range[0] > range[1])
                {
                    throw new ArgumentException("error", nameof(ranges));
                }
            }

            if (startIndex + count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if(startIndex < 0)
                throw new ArgumentOutOfRangeException(nameof(startIndex));

            int total = 0;
            for (int i = startIndex; i < startIndex + count; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    decimal[] range = ranges[j] ?? throw new ArgumentNullException(nameof(ranges));
                    if (range.Equals(Array.Empty<decimal>()))
                        continue;
                    if (arrayToSearch[i] >= range[0] && arrayToSearch[i] <= range[1])
                    {
                        total++;
                        break;
                    }
                }
            }

            return total;
        }
    }
}
